//
//  ViewController.swift
//  notifications-userdefault
//
//  Created by Freddy Bazante on 12/1/18.
//  Copyright © 2018 Freddy Bazante. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    //MARK:- Outlets
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var outputLabel: UILabel!
    
   
    
    //MARK:- ViewController lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        outputLabel.text = UserDefaults.standard.object(forKey: "mensaje") as? String
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]){(granted, error) in
            //handle error
        }
         UNUserNotificationCenter.current().delegate = self
    }
    
    
    //MARK:- Actions
    @IBAction func savedButtonPressed(_ sender: Any) {
        outputLabel.text=inputTextField.text
        UserDefaults.standard.set(outputLabel.text, forKey: "mensaje")
        setNotifications(outputLabel.text!)
    }
    
    func setNotifications(_ message:String){
        //1. importar UserNotifications
        //2. requestAuthorization
        //3. crear contenido
        let content = UNMutableNotificationContent()
        content.title = "Super título"
        content.subtitle = "Subtítulo"
        content.body = "el Mensaje es: \(message)"
        content.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
        //3.1 acciones
        let reapeatAction = UNNotificationAction(identifier: "repeat", title: "Repetir", options: [])
        let changeMessage = UNTextInputNotificationAction(identifier: "change", title: "Cambiar", options: [])
        
        //3.2 Anadir las acciones a una categoría
        let category = UNNotificationCategory(identifier: "actionsCat", actions: [reapeatAction, changeMessage], intentIdentifiers: [], options: [])
        
        //3.3 Aanadir la CategoryIdentifier al content
        content.categoryIdentifier =  "actionsCat"
        
        //3.4 Agregar el Category al UNUserNotificationCenter
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        
        
        //4. crear trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        //5. crear request
        let notificationRequest = UNNotificationRequest(identifier: "notification1", content: content, trigger: trigger)
        
        //6. anadir el request al UNUserNotificationCenter
        UNUserNotificationCenter.current().add(notificationRequest){(error) in
            //handle error
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case "repeat":
            self.setNotifications(outputLabel.text!)
            break
        case "change":
            let textReponse = response as! UNTextInputNotificationResponse
            let message = textReponse.userText
            setNotifications(message)
            break
        default:
            break
        }
        
        print(response.actionIdentifier)
        completionHandler()
    }

}

